CC = g++
CFLAGS = --std=c++11 -pthread
OBJ = main.o hilo.o misc.o measure_time.o
OBJDIR = build/obj
PROGNAME = thread_challenge
ZIPNAME = 17832733_18018294.zip
VPATH = src

all: $(addprefix build/obj/, $(OBJ))
	$(CC) $(CFLAGS) $^ -o build/$(PROGNAME)

archive: $(ZIPNAME)

$(ZIPNAME): src/*.cc src/*.hh
	git archive master --prefix=Lab2/ --format=zip > $@

# dependencias: <objeto(s)> ... : <header>.hh

$(OBJDIR)/hilo.o: hilo.hh
$(OBJDIR)/misc.o: misc.hh
$(OBJDIR)/measure_time.o: measure_time.hh
$(OBJDIR)/main_prueba.o: hilo.hh misc.hh measure_time.hh

# compilacion codigo objeto

$(OBJDIR)/%.o: %.cc | build/obj
	$(CC) $(CFLAGS) -c  $< -o $@

$(OBJDIR):
	mkdir -p build/obj


clean:
	rm -f build/obj/*.o
	rm -f build/$(PROGNAME)
