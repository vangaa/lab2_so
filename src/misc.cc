#include <fstream>
#include <string>
#include <sstream>
#include <iterator>

#include <iostream>

#include "misc.hh"
#include "hilo.hh"
#include "measure_time.hh"

using namespace std;

vector<vector<int> > inter;
vector<vector<int> > temp_inter;

pthread_barrier_t start;
pthread_mutex_t* crit_section;
pthread_barrier_t** barr_arr;

extern double* tiempos_totales;
extern double* tiempos_promedios;
extern double* tiempos_grupales;

extern int ngroups;
extern int nthreads;

vector< vector<int> > get_listas(string filename)
{
	vector< vector<int> > listas;

	ifstream fd_in(filename.data());

	if (fd_in.is_open())
	{
		string linea;
		while (! fd_in.eof())
		{
			getline(fd_in,linea);

			if (linea.length() != 0)
			{
				istringstream iss(linea);

				vector<int> lista{istream_iterator<int>{iss},
					istream_iterator<int>{}};

				listas.push_back(lista);
			}
		}
	}

	return listas;
}

void * rutina(void * objeto_hilo)
{
	hilo * thrlocal = (hilo*) objeto_hilo;

	int tid = thrlocal->getThreadID();

	//!< solo lo hace la primera hebra
	if (tid == 0)
	{
		//set tiempo para todos igual.
		tiempo_init(ngroups, nthreads);
	}


	//cout <<  tid << " Estoy en la barrera #1 "<< endl;
	pthread_barrier_wait(&start);
	//cout <<  tid << " Pase la barrera #1 "<< endl;

	while (true)
	{
		try
		{
			//for (int i = 0; i < 1000000000; i++){}//nothing
			//cout<<"hola sali del busy-waiting"<<endl;

			if(thrlocal->getCurrentList()!=1){
				resume_time(tid,thrlocal->getGroupID(), nthreads);
			}
			thrlocal->take_a_slice();
			thrlocal->ordenar_vector_local();


			vector<int> &in_vector = inter.at(thrlocal->getGroupID());
			vector<int> &out_vector = temp_inter.at(thrlocal->getGroupID());

			for (int i=0; i< in_vector.size(); i++)
			{
				int elem = in_vector[i];

				if (thrlocal->got_it(elem))
				{
					//cout << tid << " Entrando SC "<< endl;
					pthread_mutex_lock(&crit_section[thrlocal->getGroupID()]);
					//cout << tid << " Entre SC "<< endl;

					out_vector.push_back(elem);

					//cout <<  tid << " Saliendo SC "<< endl;
					pthread_mutex_unlock(&crit_section[thrlocal->getGroupID()]);
					//cout <<  tid << " Sali SC "<< endl;
				}
			}

			//cout <<  tid << " Entrando en barrera #2 "<< endl;
			pause_time(tid, thrlocal->getGroupID(), thrlocal->getCurrentList(), nthreads);
			pthread_barrier_wait(&barr_arr[0][thrlocal->getGroupID()]);
			//cout <<  tid << " Pase barrera #2 "<< endl;

			//!< solo lo hace la primera hebra
			if (tid == 0)
			{
				in_vector = vector<int>(out_vector);
				out_vector.clear();
			}

			//cout <<  tid << " Entrando en barrera #3 "<< endl;
			pthread_barrier_wait(&barr_arr[1][thrlocal->getGroupID()]);
			//cout <<  tid << " Pase barrera #3 "<< endl;
		}
		catch (hilo_error e){ break; };
	}

	pthread_exit(NULL);
}

void intersection_list_init(vector<int> &inicial, int n_grupos)
{
	for (int i=0; i< n_grupos; i++)
	{
		temp_inter.push_back(vector<int>());

		inter.push_back(inicial);
	}
}

void mutex_n_barriers_init(int ngroups, int nthreads)
{
	pthread_barrier_init(&start, NULL, ngroups*nthreads);

	crit_section = (pthread_mutex_t*) malloc(ngroups * sizeof(pthread_mutex_t));

	for (int i=0; i< ngroups; i++)
		crit_section[i] = PTHREAD_MUTEX_INITIALIZER;

	barr_arr = (pthread_barrier_t**) malloc(2*sizeof(pthread_barrier_t*));

	barr_arr[0] = (pthread_barrier_t*) malloc(ngroups * sizeof(pthread_barrier_t));
	barr_arr[1] = (pthread_barrier_t*) malloc(ngroups * sizeof(pthread_barrier_t));

	for (int i=0; i< 2; i++)
	{
		for (int j=0; j< ngroups; j++)
		{
			pthread_barrier_init(&barr_arr[i][j], NULL, nthreads);
		}
	}
}

string list_string(vector<int> lista)
{
	string temp = "[";

	for (int i=0; i< lista.size(); i++)
	{
		temp += to_string(lista[i]);
		temp += " ";
	}

	temp += "]";

	return temp;
}

void escribe_salida(string filename)
{

	ofstream fs(filename);

	int primero = PrimerGanador();
	int segundo = SegundoGanador();
	int tercero = TercerGanador();

	//cout<<"1st:"<<primero<<", 2nd:"<<segundo<<", 3th:"<<tercero<<endl;
	fs<<"Número del equipo que obtuvo el primer lugar:"<<primero<<endl;
	fs<<"Tiempo del equipo que obtuvo el primer lugar:"<<tiempos_grupales[primero]<<endl;
	fs<<"Número del equipo que obtuvo el segundo lugar:"<<segundo<<endl;
	fs<<"Tiempo del equipo que obtuvo el segundo lugar:"<<tiempos_grupales[segundo]<<endl;
	fs<<"Número del equipo que obtuvo el tercer lugar:"<<tercero<<endl;
	fs<<"Tiempo del equipo que obtuvo el tercer lugar:"<<tiempos_grupales[tercero]<<endl;

	int mejorhebra = MejorHebra();
	fs<<"Hebra más eficiente:"<<mejorhebra<<endl;

	int hebrapromedio = MejorHebraPromedio();
	fs<<"Hebra más eficiente en promedio:"<<hebrapromedio<<endl;

	fs<< "Intersecciones: "<< endl;

	for (int i=0; i< ngroups; i++)
	{
		fs << "S"<< to_string(i)<< ": "<< list_string(inter[i]) << endl;
	}

	fs.close();
}
