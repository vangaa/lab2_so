#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <string>

#include "misc.hh"
#include "hilo.hh"

using namespace std;

int ngroups;
int nthreads;
extern vector<vector<int> > inter;

enum err_opt{WRONG_OPT, ZERO_G_OR_T, INPUT_MISS, ARGS_MISS};

inline string usage(string progname)
{
	return
		"uso: "+progname+" OPCIONES <fichero_entrada> \n"
		"\n"
		"opciones:\n"
		"  -g <Numero>\n"
		"       Numero de grupos que participaran en la \"competencia\"\n\n"
		"  -h <Numero>\n"
		"       Numero de hebras por grupo\n\n"
		"  <fichero_entrada>\n"
		"       Es el fichero con las listas de numeros que procesaran\n"
		"       cada uno de los grupos que participan\n";
}

int main(int argc, char* argv[])
{
	string optstr = "g:h:";

	int groupnum, threadsnum;
	int opt;

	if (argc < 2)   // sin argumentos
	{
		cout << usage(argv[0])<< endl;
		return 0;
	}


	else	   // Parseo de argumentos
	{
		while ((opt = getopt(argc, argv, optstr.data())) != -1)
		{
			switch (opt)
			{
				case 'g':
					groupnum = atoi(optarg);
					ngroups=groupnum;
					break;
				case 'h':
					threadsnum = atoi(optarg);
					nthreads = threadsnum;
					break;
				default:
					cerr << "Ejecute sin argumentos para ver el uso."<< endl;
					return WRONG_OPT;
			}
		}

		if (groupnum <= 0 || threadsnum <= 0)
		{
			cerr << "Parametros muy bajos"<< endl;
			return ZERO_G_OR_T;
		}

		if (optind >= argc)
		{
			cerr << "Falta el archivo de entrada"<< endl;
			return INPUT_MISS;
		}
		else if (optind < 5)
		{
			cerr << "Faltan argumentos"<< endl;
			return ARGS_MISS;
		}
	}

	string inputname = argv[optind];

	// Opciones seteadas

	hilo::tabla = get_listas(inputname);

	mutex_n_barriers_init(groupnum, threadsnum);
	intersection_list_init(hilo::tabla[0], groupnum);

	pthread_t* threads = (pthread_t*) malloc(groupnum * threadsnum * sizeof(pthread_t));
	int count_th = 0;

	// se crean hebras
	for (int i=0; i< groupnum; i++)
	{
		for (int j=0; j< threadsnum; j++)
		{
			pthread_create(&threads[count_th++], NULL, &rutina, (void*)new hilo(i,j));
		}
	}

	// se espera a las hebras
	for (int i=0; i< groupnum*threadsnum; i++)
	{
		pthread_join(threads[i], NULL);
	}

	calc_tiempos_grupales(groupnum, threadsnum);

	escribe_salida("resultado.txt");

	return 0;
}
