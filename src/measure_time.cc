#include "measure_time.hh"


using namespace std;

//time_t* tiempos_inicios;
//time_t* tiempos_finales;
//double* tiempos_totales;
//double* tiempos_promedios;
//double* tiempos_grupales;


chrono::time_point<chrono::system_clock>* tiempos_inicios;
chrono::time_point<chrono::system_clock>* tiempos_finales;
chrono::duration<double>* tiempos_totales;
chrono::duration<double>* tiempos_promedios;
chrono::duration<double>* tiempos_grupales;


extern int ngroups;
extern int nthreads;



void tiempo_init(int ngroups, int nthreads){
	tiempos_inicios = (chrono::time_point<chrono::system_clock>*) malloc(ngroups * nthreads * sizeof(chrono::time_point<chrono::system_clock>));
	tiempos_finales = (chrono::time_point<chrono::system_clock>*) malloc(ngroups * nthreads * sizeof(chrono::time_point<chrono::system_clock>));
	tiempos_promedios = (chrono::duration<double>*) malloc(ngroups * nthreads * sizeof(chrono::duration<double>));
	tiempos_totales = (chrono::duration<double>*) malloc(ngroups * nthreads * sizeof(chrono::duration<double>));
	tiempos_grupales = (chrono::duration<double>*) malloc(ngroups * sizeof(chrono::duration<double>));
	
	chrono::time_point<chrono::system_clock> actual = chrono::system_clock::now();
	//time_t actual = time(&actual);	//tiempo actual.
	for (int i = 0; i < ngroups*nthreads; i++)
	{
		tiempos_inicios[i] = actual;
		//tiempos_finales[i] = 0;
		//tiempos_promedios[i] = 0;
		//tiempos_totales[i] = 0;
	}
	//cout<<"init: tiempos_inicios[0]:"<<tiempos_inicios[0]<<endl;
}

void pause_time(int id, int group, int linea_actual, int nthreads){
	int i = nthreads*group + id;
	tiempos_finales[i] = chrono::system_clock::now();
	//time(&(tiempos_finales[i]));
	//tiempos_totales[i] += difftime (tiempos_finales[i],tiempos_inicios[i]);
	tiempos_totales[i] += tiempos_finales[i]-tiempos_inicios[i];

	//cout<<"la:"<<linea_actual<<endl;
	//cout<<"paused: **difftime="<<difftime (tiempos_finales[i],tiempos_inicios[i])<<endl;
	//cout<<"paused: **tiempos_promedios["<<i<<"]:"<<tiempos_promedios[i]<<endl;
	//cout<<"paused: **la-2="<<linea_actual-2<<endl;
	//cout<<"paused: **tprom*(la-2)="<<(tiempos_promedios[i] * (linea_actual-2))<<endl;
	//cout<<"paused: **tprom*(la-s) + ttotal="<<((tiempos_promedios[i] * (linea_actual-2)) + tiempos_totales[i])<<endl;
	//tiempos_promedios[i] = ((tiempos_promedios[i] * (linea_actual-2)) + difftime(tiempos_finales[i],tiempos_inicios[i]))/ (linea_actual-1);
	tiempos_promedios[i] = ((tiempos_promedios[i] * (linea_actual-2)) + (tiempos_finales[i]-tiempos_inicios[i])) / (linea_actual-1);

	//cout<<"paused: tiempos_finales["<<i<<"]:"<<tiempos_finales[i]<<endl;
	//cout<<"paused: tiempos_totales["<<i<<"]:"<<tiempos_totales[i]<<endl;
	//cout<<"paused: tiempos_promedios["<<i<<"]:"<<tiempos_promedios[i]<<endl;
}

void resume_time(int id, int group, int nthreads){
	int i = nthreads*group + id;
	//time(&(tiempos_inicios[i]));
	tiempos_inicios[i] = chrono::system_clock::now();
	//cout<<"resume_time: tiempos_inicios["<<i<<"]:"<<tiempos_inicios[i]<<endl;
}

void calc_tiempos_grupales(int ngroups, int nthreads){
	for (int i=0; i < ngroups; i++)
	{
		chrono::duration<double> max = chrono::duration<double>(0);	
		for (int j = 0; j < nthreads; j++)
		{
			if(tiempos_totales[i*nthreads+j]>max){
				max = tiempos_totales[i*nthreads+j];
			}
		}
		tiempos_grupales[i] = max;
		//cout<<"grupal: tiempos_grupales["<<i<<"]:"<<tiempos_grupales[i]<<endl;
	}
}

int PrimerGanador(){
	chrono::duration<double> min = tiempos_grupales[0];
	int pos=0;
	for (int i = 0; i < ngroups; i++)
	{
		if(tiempos_grupales[i]<min){
			min = tiempos_grupales[i];
			pos = i;
		}
	}
	return pos;
}

int SegundoGanador(){
	int primer = PrimerGanador();
	chrono::duration<double> max = chrono::duration<double>(0);
	for (int i = 0; i < ngroups; i++)
	{
		if(tiempos_grupales[i]>max){
			max= tiempos_grupales[i];
		}	
	}

	chrono::duration<double> min = max + chrono::duration<double>(1);
	int pos=primer;
	for (int i = 0; i < ngroups; i++)
	{
		if (tiempos_grupales[i]<=min and i!=primer){
			min = tiempos_grupales[i];
			pos = i;
		}
	}
	return pos;
}

int TercerGanador(){
	int primer = PrimerGanador();
	int segundo = SegundoGanador();
	chrono::duration<double> max = chrono::duration<double>(0);
	for (int i = 0; i < ngroups; i++)
	{
		if(tiempos_grupales[i]>max){
			max= tiempos_grupales[i];
		}	
	}

	chrono::duration<double> min = max + chrono::duration<double>(1);
	int pos = segundo;
	for (int i = 0; i < ngroups; i++)
	{
		if(tiempos_grupales[i]<=min and i!=primer and i!=segundo){
				min = tiempos_grupales[i];
				pos = i;
		}
	}
	return pos;
}


int MejorHebra(){
	chrono::duration<double> min = tiempos_totales[0];
	int pos=0;
	for (int i = 0; i < ngroups*nthreads; i++)
	{
		if(tiempos_totales[i]<min){
			min=tiempos_totales[i];
			pos=i;
		}
	}
	return pos;
}


int MejorHebraPromedio(){
	chrono::duration<double> min = tiempos_promedios [0];
	int pos=0;
	for (int i = 0; i < ngroups*nthreads; i++)
	{
		if(tiempos_promedios[i]<min){
			min=tiempos_promedios[i];
			pos=i;
		}
	}
	return pos;
}

