#ifndef MISC_H
#define MISC_H

#include <vector>
#include <time.h>

using namespace std;


/**
 * Lee un fichero de entrada con las listas que hay que intersectar y las
 * convierte en una tabla o una listas de listas.
 *
 * @param filename Es el nombre del fichero del cual se sacan las listas
 *
 * @return Un vector de vectores que guarda todas las listas escritas en el
 * archivo.
 */
vector< vector<int> > get_listas(string filename);


/**
 * Rutina que realizan todas las hebras. Cada hebra busca en el vector 'inter'
 * los elementos que coincidan con su vector local, que es un trozo del vector
 * K. Los elementos que coinciden se guardan en el vector 'temp_inter', y
 * cuando todas las hebras terminan, se copian los elementos de 'temp_inter' a
 * 'inter' y se borran los elementos de 'temp_inter'. Esto se hace hasta que
 * no queden mas vectores que intersectar.
 *
 * @param objeto_hilo Es la instancia de la clase 'hilo' que hace la rutina.
 */
void * rutina(void * objeto_hilo);

/**
 * Inicializa los vectores de interseccion temporal y final.
 *
 * @param inicial Es el vector S inicial el cual los grupos revisaran por
 * coincidencias.
 * @param n_grupos Es la cantidad de grupos.
 */
void intersection_list_init(vector<int> &inicial, int n_grupos);

/**
 * Inicializa las barreras y mutex usados en la rutina concurrente que
 * realizan las hebras.
 *
 * @param ngroups cantidad de grupos que participan
 * @param nthreads cantidad de hebras que participan
 */
void mutex_n_barriers_init(int ngroups, int nthreads);


 /**
  * Inicializa los tiempos globales.
  *	@param ngroups numero de grupos.
  * @param nthreads numero de hilos.
  */
void tiempo_init(int ngroups, int nthreads);

 /**
  * Detiene la cuenta de tiempo para ese hilo,
  * y actualiza el tiempo promedio.
  *	@param i id del hilo.
  * @param group grupo del hilo
  * @param linea_actual iteración en la que se encuentra.
  * @param nthreads numero de threads.
  */
void pause_time(int id, int group, int linea_actual, int nthreads);

 /**
  * Reanuda la cuenta de tiempo para ese hilo
  * @param i id del hilo.
  * @param group grupo del hilo
  * @param nthreads numero de hilos.
  */
void resume_time(int id, int group, int nthreads);


/**
 * Calcula el tiempo por grupo
 * @param ngroups numero de grupos.
 * @param nthreads numero de hilos.
 */
void calc_tiempos_grupales(int ngroups, int nthreads);

/**
 * Devuelve la representacion de una lista a un string.
 *
 * @param lista Es la lista que se quiere representar
 *
 * @return Un string en representacion de la lista. Este string tiene la
 * forma: [a0 a1 a2 ...], donde a0, a1, etc, son los elementos de la lista.
 */
string list_string(vector<int> lista);

/**
 * Escribe lo pedido en el archivo de salida 'filename'.
 *
 * @param filename Es el nombre del archivo de salida
 */
void escribe_salida(string filename);

int PrimerGanador();
int SegundoGanador();
int TercerGanador();

int MejorHebra();
int MejorHebraPromedio();

#endif
