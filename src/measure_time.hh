#ifndef M_TIME_H
#define M_TIME_H

//#include <time.h>
#include <stdlib.h>
#include <chrono>
#include <ctime>
using namespace std;

 /**
  * Inicializa los tiempos globales.
  *	@param ngroups numero de grupos.
  * @param nthreads numero de hilos. 
  */
void tiempo_init(int ngroups, int nthreads);

 /**
  * Detiene la cuenta de tiempo para ese hilo,
  * y actualiza el tiempo promedio.
  *	@param i id del hilo.
  * @param group grupo del hilo
  * @param linea_actual iteración en la que se encuentra.
  * @param nthreads numero de threads.
  */
void pause_time(int id, int group, int linea_actual, int nthreads);

 /**
  * Reanuda la cuenta de tiempo para ese hilo
  * @param i id del hilo.
  * @param group grupo del hilo
  * @param nthreads numero de hilos.
  */
void resume_time(int id, int group, int nthreads);


/**
 * Calcula el tiempo por grupo
 * @param ngroups numero de grupos.
 * @param nthreads numero de hilos.
 */
void calc_tiempos_grupales(int ngroups, int nthreads);


int PrimerGanador();
int SegundoGanador();
int TercerGanador();

int MejorHebra();
int MejorHebraPromedio();


#endif