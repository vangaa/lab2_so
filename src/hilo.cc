#include "hilo.hh"

int hilo::thread_count = 0;
vector<vector<int> > hilo::tabla;

hilo::hilo(int group, int id)
{
    this->gr_id = group;
    this->th_id = id;
    this->current_list = 1;

    if (id+1 > thread_count)
	this->thread_count++;
}

void hilo::combinar(vector<int> &array, int inicio, int medio, int fin)
{
    vector<int> aux;
    int indAux, indFst, indSnd;
    int i;

    indFst = inicio;    //<! indice de la primera mitad
    indSnd = medio+1;   //<! indice de la segunda mitad

    while (indFst <= medio && indSnd <= fin)
    {
	//  cambiar <= por >= si se quiere ordenar de mayor a menor
	if (array[indFst] <= array[indSnd])
	{
	    aux.push_back(array[indFst++]);
	}
	else
	{
	    aux.push_back(array[indSnd++]);
	}
    }

    // Se copian los elementos de la primera mitad no comparados

    while (indFst<=medio)
    {
	aux.push_back(array[indFst++]);
    }

    // Se copian los elementos de la segunda mitad no comparados

    while (indSnd <= fin)
    {
	aux.push_back(array[indSnd++]);
    }

    indAux = 0;

    /* Finalmente se copian los elementos del array auxiliar (ordenados)
       en el array original */

    for (i = inicio; i <= fin; i++)
    {
	array[i] = aux[indAux++];
    }
}

void hilo::merge_sort(vector<int> &array, int inicio, int fin)
{
    int medio = (inicio + fin)>>1;

    if (inicio < fin)
    {
	merge_sort(array, inicio, medio);
	merge_sort(array, medio +1, fin);
	combinar(array, inicio, medio, fin);
    }
}

void hilo::ordenar(vector<int> &lista, int from, int till)
{
    if (from < till && till < lista.size())
	merge_sort(lista, from, till);
}

void hilo::ordenar(vector<int> &lista)
{
    merge_sort(lista, 0, lista.size()-1);
}

void hilo::ordenar_vector_local()
{
    ordenar(miparte);
}

bool hilo::busqueda_binaria(vector<int> v, int x)
{
	int a = 0;
	int b = v.size();
	return busqueda_binaria(v,a,b,x);
}

bool hilo::busqueda_binaria(vector<int> v, int a, int b, int x)
{
	int med;
	while(a!=b){
		med = a + ((b-a)/2);
		if(v[med] == x){
			return true;
		}
		if(x<v[med]){
			b = med;
		}
		else{
			a = med + 1;
		}
	}
	return (v[b] == x);
}

void hilo::take_a_slice() throw(hilo_error)
{
    if (current_list >= tabla.size()) throw EMPTY_VECTOR;

    vector<int> &ref = tabla.at(current_list++); 

    unsigned int portion_size = ref.size()/thread_count;

    if (portion_size == 0 || (ref.size() % portion_size) != 0 )
	portion_size++;

    int init = portion_size * th_id;
    int end = init + portion_size;

    if (end > ref.size()) end = ref.size();

    if (end > init)
	miparte = vector<int>(ref.begin()+init, ref.begin()+end);
}

vector<int> hilo::get_local_vector()
{
    return miparte;
}

bool hilo::got_it(int elem)
{
    bool do_I = busqueda_binaria(miparte, elem);

    return do_I;
}
