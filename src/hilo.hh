#ifndef HILO_H
#define HILO_H

#include <vector>

using namespace std;

enum hilo_error{EMPTY_VECTOR};

class hilo {
	private:
		int gr_id, th_id;
		int current_list;	    //!< Indica que lista de la tabla le toca
		static int thread_count;    //!< Hebras por grupo

		vector<int> miparte;	//!< trozo de la lista K que le corresponde

		/**
		  Subfuncion de mergesort. Combina las subsoluciones del algoritmo de
		  ordenamiento por mezcla.
		  @param array Es el array original en donde se copiaran las
		  subsoluciones ordenadas.
		  @param inicio Es el inicio del primer subarray
		  @param medio Es el fin del primer subarray
		  @param fin Es el fin del segundo subarray
		 */
		void combinar(vector<int> &array, int inicio, int medio, int fin);

		/**
		 * Ordena una lista de menor a mayor.
		 * @param array[] Es el array que se ordena
		 * @param inicio De donde comienza a ordenar
		 * @param fin Hasta donde ordena
		 */
		void merge_sort(vector<int> &array, int inicio, int fin);

	public:

		static vector< vector<int> > tabla;  //!< Contiene las listas a ordenar

		hilo(int group, int id);

		/**
		 * Ordena un vector de enteros de menor a mayor. El algoritmo de
		 * ordenamiento es el algoritmo por mezcla.
		 *
		 * @param lista
		 */
		void ordenar(vector<int> &lista);

		/**
		 * Busca en un vector de enteros utilizando busqueda binaria.
		 *
		 * @param v vector donde buscar.
		 * @param x entero a buscar.
		 * @return true si es que está, false en caso contrario.
		 */
		bool busqueda_binaria(vector<int> v, int x);

		/**
		 * Busca en un vector de enteros utilizando busqueda binaria.
		 *
		 * @param v vector donde buscar.
		 * @param a indice donde empieza a buscar.
		 * @param b indice donde termina de buscar(incluyendolo).
		 * @param x entero a buscar.
		 * @return true si es que está, false en caso contrario.
		 */
		bool busqueda_binaria(vector<int> v, int a, int b, int x);

		/**
		 * Dice si un elemento pertenece a su vector local o no.
		 *
		 * @param elem es el elemento que se busca.
		 *
		 * @return true si lo tiene; false en caso contrario
		 */
		bool got_it(int elem);

		/**
		 * Ordena el trozo que le toco de el arreglo total.
		 */
		void ordenar_vector_local();

		/**
		 * Ordena un vector desde from hasta till.
		 *
		 * @param lista Lista que ordena
		 * @param from Desde donde ordena
		 * @param till Hasta donde ordena
		 */
		void ordenar(vector<int> &lista, int from, int till);

		/**
		 * Obtiene el trozo de vector que le corresponde segun el numero de id
		 * del hilo. Este vector lo obtiene de la tabla de vectores leida del
		 * archivo de entrada el cual 'corta' solo el pedazo que necesita
		 * hacia su vector local
		 *
		 * @throw hilo_error En caso que el vector de origen no exista o esta
		 * vacio.
		 */
		void take_a_slice() throw(hilo_error);

		/**
		 * Devuelve el trozo de vector que le corresponde.
		 *
		 * @return Su vector
		 */
		vector<int> get_local_vector();

		int getGroupID() { return gr_id; };
		int getThreadID() { return th_id; };
		int getCurrentList() {return current_list;};
};

#endif
