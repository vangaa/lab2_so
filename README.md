# LABORATORIO 2

--- 

## OBJETIVO

Se tiene un archivo de entrada con *n* lineas en donde cada linea representa
una lista de numeros. El objetivo del programa es buscar la interseccion entre
esas *n* listas. Para ello se especifica una cantidad de grupos **G** y una
cantidad de hebras por grupo **T**. Estos grupos compiten entre si para saber
quien lo hace primero.

Para lograrlo, se selecciona la lista con menor longitud **S** y la siguiente
con menor longitud **K**. Cada grupo divide la lista **K** en **T** trozos y le
asigna cada trozo a una hebra, la cual lo ordena de menor a mayor y busca de
forma binaria los elementos en común, entre **S** y **K**. Esto se repite hasta
encontrar todos los elementos que las listas tienen en común.  

## COMPILACIÓN

Ubiquese en la carpeta principal del proyecto que es donde se encuentra el
Makefile y las carpetas 'src/' y 'build/', y ejecute el comando de GNU/Linux
`make` para compilar automaticamente el programa. El(los) ejecutables quedaran
en la carpeta 'build/' y el codigo objeto en la carpeta 'build/obj'.


## EJECUCIÓN

Ubiquese en la carpeta 'build/' del proyecto y ejecute el programa con:

	./thread_challenge -g <numero grupos> -h <numero threads> <archivo entrada>

ejecute sin opciones (`./thread_challenge`) para mas informacion.

## AUTORES

Los autores principales del proyecto son:

 - Luis Loyola
 - Ian Mejias

